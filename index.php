<?php 
session_start();
$newCash = $_SESSION['currency'];

$cash = [
    'uah' =>[
       'name' => 'Гривна',
       'course' => 1,
    ],
    'usd' =>[
       'name' => 'Доллар',
       'course' => 27.1,
    ],
    'eur'=> [
       'name' => 'Евро',
       'course' => 30.2,
    ],
 ];

$date = [
   [
      'title' => 'Бегония',
      'price_val' => 300,
    ],
    [
      'title' => 'Белый лотос',
      'price_val' => 400,
    ],
    [
      'title' => 'Бромелия',
      'price_val' => 455,
    ],
    [
      'title' => 'Георгин',
      'price_val' => 430,
    ],
    [
      'title' => 'Желтый гибискус',
      'price_val' => 2040,
    ],
    [
      'title' => 'Иксора',
      'price_val' => 50,
    ],
    [
      'title' => 'Канна',
      'price_val' => 3000,
    ],
    [
      'title' => 'Лантана',
      'price_val' => 600,
    ],
    [
      'title' => 'Орхидея',
      'price_val' => 800,
    ],
    [
      'title' => 'Плюмерия',
      'price_val' => 350,
    ],
   ];

 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
	
 </head>
 <body>
 <div class="container">
 <form action="/setup.php" method="POST">
      <label>
         <select class="form-select" name="currency">
               <option <?php if($cash['name'] == null):?> selected <?php endif;?> value='uah'>Гривна</option>
               <option value='usd'>Доллар</option>
               <option value='eur'>Евро</option>
         </select>
      </label>
      <button class="btn-warning">Refresh</button>
</form>
</div>
<div class="container">
<table class="table table-striped">
   <tr>
      <th>Title</th>
      <th>Price</th>
   </tr>
   <?php foreach ($cash as $key => $x): ?>
      <?php if($key == $newCash):?> 
<?php foreach ($date as $shop): ?>
   <tr>
   <td><?=$shop['title']?></td>
   <td><?=$shop['price_val'] / $cash[$key] ['course']. ' ' . $cash[$key]['name']?></td>
   </tr>
   <?php endforeach;?>
   <?php endif;?>    
<?php endforeach; ?>
</table>
</div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	
 </body>
 </html>